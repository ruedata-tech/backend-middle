# Prueba técnica
## Cargo: Desarrollador backend
## Nivel: Middle

En la siguiente prueba se evualarán tus habilidades técnicas actuales y tu capacidad para resolver problemas, cada punto será evaluado de forma independiente y se tendrá en cuenta no solo el resultado sino también la calidad de la solución.

Para poder evaluar las soluciones **por favor súbelas a un repositorio de Github** con las instrucciones paso a paso para poder ser ejecutadas de manera local.

**Nota**: En el correo en el que recibiste esta prueba técnica se especifica el lenguaje en el cual se debe desarrollar y el tipo de base de datos a utilizar. En caso de alguna duda por favor comunícate al siguiente correo: a.romero@ruedata.com

## Ejercicio 1 - Autenticación con JWT

En un ambiente dockerizado desarrolla un API que implemente el flujo completo de autenticación de usuarios por medio de JWT (json web tokens) del lado del backend.
Realiza los módulos, validaciones y prácticas que creas pertinentes para el ejercicio.

## Ejercicio 2 - Consumo de API y cron

En un ambiente dockerizado desarrolla un cron que consuma el API https://anapioficeandfire.com/ y almacene en la base de datos cada 30 minutos el nombre y el género de un personaje aleatorio.

## Ejercicio 3 - Algoritmo

Realiza dos implementaciones de un algoritmo que dado un número n calcule el n-avo número de la serie fibonacci.
